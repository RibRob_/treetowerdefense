using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Health))]
public class EnemyBase : MonoBehaviour {

    public float speed = 1f;
    public float damage = 1f;
    public int currency = 1;
    public float attackRate = 1f;
    public CharacterController characterController;
    public GameObject deathParticlePrefab;
    public AudioClip deathClip;

    // Start is called before the first frame update
    void Start() {
        
    }

    // Update is called once per frame
    void Update() {
        
    }

    public void Attack() { 
    }

    public void Die() { 
    }
}
