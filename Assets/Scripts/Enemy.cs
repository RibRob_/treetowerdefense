using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Health))]
public class Enemy : MonoBehaviour
{
    public float speed = 1f;
    public int currency = 1;
    public CharacterController characterController;
    public GameObject deathParticlePrefab;
    public AudioClip deathClip;

    private bool isDead = false;
    public bool IsDead { get { return isDead; } }

    int currentWaypoint = 0;
    float WaypointRadius = 1;
    List<Vector3> waypoints = new List<Vector3>();

    public List<Vector3> Waypoints { set { waypoints = value; } }

    private Animator animatore;

    // Start is called before the first frame update
    void Start()
    {
        animatore = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    public void Attack()
    {
        animatore.SetBool("isAttacking", true);
        speed *= 2;

    }

    public void Move()
    {
        if (waypoints.Count <= 0) { return; }


        if (Vector3.Distance(waypoints[currentWaypoint], transform.position) < WaypointRadius)
        {
            currentWaypoint++;
            if (currentWaypoint >= waypoints.Count-1)
            {
                Attack();
                currentWaypoint = waypoints.Count-1;
            }

        }

        transform.LookAt(waypoints[currentWaypoint]);
        transform.position = Vector3.MoveTowards(transform.position, waypoints[currentWaypoint], Time.deltaTime * speed);
    }

    public void Die()
    {
        this.gameObject.SetActive(false);
        print("dead");
        isDead = true;
    }
}
