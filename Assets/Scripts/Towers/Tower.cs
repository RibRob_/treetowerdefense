using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.InputSystem.XR;

public abstract class Tower : MonoBehaviour
{

    public float range;
    public float fireRate;
    public float damage;
    protected float count = 0;
    public int upgradeCost = 50;
    //protected int statusEffect;
    //protected float splashDamage;

    [SerializeField] protected Transform shotSpawn;
    [SerializeField] protected GameObject prefab;

    private Animator animatore;
    private CharacterController controller;

    [SerializeField] float speed;


    private void Start()
    {
        animatore = GetComponent<Animator>();
        controller = GetComponent<CharacterController>();
    }

    private void Update()
    {
        EnemyCheck();

        count += (count <= fireRate) ? Time.deltaTime : 0;


        if (Input.GetKey(KeyCode.W) && !animatore.GetBool("isRooted"))
        {
            animatore.SetBool("isMoving", true);
            controller.Move(Vector3.forward * Time.deltaTime * speed);
        }
        else
        {
            animatore.SetBool("isMoving", false);
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            animatore.SetBool("isRooted", true);
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            animatore.SetBool("isRooted", false);
        }

    }

    public void ShootWithSpacebar()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //ShootAtEnemy();
        }
    }

    public void ShootAtEnemy(Transform enemy)
    {
        //transform.LookAt(enemy.position);

        if (count > fireRate)
        {
            //animatore.SetBool("isAttacking", true);
            Instantiate(prefab, shotSpawn.position, Quaternion.identity).GetComponent<Projectile>().setPerams(shotSpawn, enemy);
            count = 0;
        }
    }

    // uses Physics.OverlapSphere to Compute and store colliders touching or inside the sphere.
    public void EnemyCheck()
    {
        Collider[] hitColliders = Physics.OverlapSphere(this.transform.position, range);
        foreach (Collider collider in hitColliders)
        {
            Enemy enemy = collider.GetComponent<Enemy>();
            if (enemy != null)
            {
                ShootAtEnemy(enemy.transform);
            }
        }
    }

}
