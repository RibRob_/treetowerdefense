using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] float fireRate;
    [SerializeField] float statusEffect;
    [SerializeField] int damage;
    [SerializeField] float splashDamage;
    [SerializeField] float velocity;
    [SerializeField] Vector3 shotVector;

    [SerializeField] GameObject explosionPrefab;

    private Transform shotSpawn;
    private Transform enemy;
    private Vector3 enemyPos;


    private float interpolateAmount;

    public void setPerams(Transform shotSpawn, Transform enemy)
    {
        this.shotSpawn = shotSpawn;
        this.enemy = enemy;
    }

    void Update()
    {
        if (shotSpawn == null && enemy == null)
        {
            return;
        }

        if (!enemy.GetComponent<Enemy>().IsDead)
        {
            enemyPos = enemy.transform.position;
        }

        if (interpolateAmount < 1f)
        {
            interpolateAmount = (interpolateAmount + Time.deltaTime*velocity);
            this.transform.position = Vector3.Lerp(shotSpawn.position, enemyPos, interpolateAmount);
        }
        else
        {
            enemy.GetComponent<Health>().TakeDamage(damage);
            Destroy(Instantiate(explosionPrefab,transform.position,Quaternion.identity), 2);
            Destroy(this.gameObject);
        }
    }
    
}