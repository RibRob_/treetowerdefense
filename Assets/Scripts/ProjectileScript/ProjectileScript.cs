using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileScript : MonoBehaviour
{
    private Rigidbody rb;
    private StatsComponent sc;
    private GameObject tower;

    public GameObject Tower
    {
        get { return tower; }
        set { tower = Tower; }
    }

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        sc = GetComponent<StatsComponent>();
        tower = GetComponent<GameObject>(); // Try to add ".GetComponent<ProjectileScript>().Tower = gameObject;" to the end of the instantiate function
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rb.velocity = tower.transform.forward * sc.speed;
    }

    public void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Enemy"))
        {
            other.GetComponent<StatsComponent>().Health -= sc.damage;
        }
    }
}
