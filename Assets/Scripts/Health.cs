using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour
{

    [SerializeField] public int currentHealth;
    [SerializeField] public int maxHealth;
    public UnityEvent Death;
    public UnityEvent TookDamage;

    // Update is called once per frame
    void Update() {
        CheckHealth();
    }

    private void CheckHealth() {
        if (currentHealth <= 0) { 
            Death.Invoke();
        }
        else if (currentHealth > maxHealth) {
            currentHealth = maxHealth;
        }
    }

    public void TakeDamage(int damage) { 
        currentHealth -= damage;
        TookDamage.Invoke();
    }
}
