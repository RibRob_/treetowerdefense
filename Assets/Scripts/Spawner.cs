using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    [SerializeField] int enemyCount;
    [SerializeField] List<GameObject> enemyPrefabs = new List<GameObject>();
    [SerializeField] List<Enemy> enemyPool = new List<Enemy>();

    [SerializeField] Track track;

    float count = 0f;
    float delay = 2f;
    int sendCount = 0;


    private void Start()
    {
        for (int i = 0; i < enemyCount; i++)
        {
            enemyPool.Add(Instantiate(enemyPrefabs[0],transform.position,Quaternion.identity).GetComponent<Enemy>());
            enemyPool[i].gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        count += (count < delay) ? Time.deltaTime : 0;

        if (count >= delay)
        {
            Send();
            count = 0;
            
        }
    }

    void Send()
    {
        sendCount %= enemyCount;
        enemyPool[sendCount].gameObject.SetActive(true);
        enemyPool[sendCount].Waypoints = track.getAllPoints();
        sendCount++;
    }
}
