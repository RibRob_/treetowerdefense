using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Cinemachine;
using UnityEngine.Events;
using DG.Tweening;
using Unity.VisualScripting;

public class CameraController : MonoBehaviour {

    [SerializeField] public List<Transform> turretTransforms = new List<Transform>();
    [SerializeField] private Transform constantTarget;
    [SerializeField] private float swapDuration = 1f;
    [SerializeField] private int currentIndex = 0;
    [SerializeField] private bool following = false;
    [SerializeField] private bool swapReady = true;
    [SerializeField] private CinemachineFreeLook cmCamera;
    [SerializeField] private float rotateSpeed = 100f;
    private InputAction.CallbackContext rotateContext;
    [SerializeField] private float scrollSensitivity = 50f;
    private Coroutine currentFollowSequence = null;

    public UnityEvent swappedUp;
    public UnityEvent swappedDown;
    public UnityEvent<Transform> swapUpdate;

    private void Update() {
        DoRotate();
        Follow();
    }

    public void Follow() {
        if (turretTransforms[currentIndex].tag == "Tower" && following) {
            constantTarget.position = turretTransforms[currentIndex].position;
        }
    }

    public void OnSwap(InputAction.CallbackContext inputContext) {
        if (currentFollowSequence != null) {
            StopCoroutine(currentFollowSequence);
        }
        following = false;

        //Get the acutal value of the input
        float inputValue = inputContext.ReadValue<float>();

        if (inputContext.performed && inputValue > 0 && swapReady) {
            currentIndex++;
            CheckCurrentIndex();
            swapReady = false;
            swappedUp.Invoke();
        }
        else if (inputContext.performed && inputValue < 0 && swapReady) { 
            currentIndex--;
            CheckCurrentIndex();
            swapReady = false;
            swappedDown.Invoke();
        }

        if (inputContext.canceled) { 
            swapReady = true;
        }

        constantTarget.DOMove(turretTransforms[currentIndex].position, swapDuration);
        currentFollowSequence = StartCoroutine(FollowSequence());
        swapUpdate.Invoke(turretTransforms[currentIndex]);
    }

    private IEnumerator FollowSequence() { 
        yield return new WaitForSeconds(swapDuration);
        following = true;
    }

    private void CheckCurrentIndex() {
        if (currentIndex > turretTransforms.Count - 1) { 
            currentIndex = 0;
        }
        else if (currentIndex < 0) {
            currentIndex = turretTransforms.Count - 1;
        }
    }

    public void OnRotate(InputAction.CallbackContext inputContext) {
        rotateContext = inputContext;
    }

    private void DoRotate() {
        //Get the acutal value of the input
        float inputValue = rotateContext.ReadValue<float>();

        //Turn Right
        if ((rotateContext.started || rotateContext.performed) && inputValue > 0) {
            cmCamera.m_XAxis.Value += Time.deltaTime * rotateSpeed;
        }
        //Turn Left
        else if ((rotateContext.started || rotateContext.performed) && inputValue < 0) {
            cmCamera.m_XAxis.Value -= Time.deltaTime * rotateSpeed;
        }
    }

    public void OnZoom(InputAction.CallbackContext inputContext) {
        //Get the acutal value of the input
        float inputValue = inputContext.ReadValue<float>();

        if (inputValue > 0) {
            cmCamera.m_Orbits[0].m_Radius += Time.deltaTime * scrollSensitivity;
            cmCamera.m_Orbits[0].m_Height += Time.deltaTime * scrollSensitivity;
            cmCamera.m_Orbits[1].m_Radius += Time.deltaTime * scrollSensitivity;
            cmCamera.m_Orbits[1].m_Height += Time.deltaTime * scrollSensitivity;
        }
        else if (inputValue < 0) {
            cmCamera.m_Orbits[0].m_Radius -= Time.deltaTime * scrollSensitivity;
            cmCamera.m_Orbits[0].m_Height -= Time.deltaTime * scrollSensitivity;
            cmCamera.m_Orbits[1].m_Radius -= Time.deltaTime * scrollSensitivity;
            cmCamera.m_Orbits[1].m_Height -= Time.deltaTime * scrollSensitivity;
        }
    }
}
