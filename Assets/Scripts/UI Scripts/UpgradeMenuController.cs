using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.InputSystem;
using DG.Tweening;

public class UpgradeMenuController : MonoBehaviour {

    public int currency = 300;
    public int newTowerCost = 200;
    public static UpgradeMenuController instance;
    public RectTransform towerUpgradeMenuContainer;
    public RectTransform gardenMenuContainer;
    public Transform currentTarget;
    public Tower currentTower;
    public bool onGardenMenu = true;
    public GameObject towerPrefab;
    public List<Transform> spawnPoints = new List<Transform> ();
    public CameraController cameraController;

    //Range Upgarde Button
    public TextMeshProUGUI towerCostTMPro;
    public TextMeshProUGUI rangeCostTMPro;
    public TextMeshProUGUI fireRateCostTMPro;
    public TextMeshProUGUI damageCostTMPro;

    private void Awake() {
        instance = this;
        towerCostTMPro.text = newTowerCost.ToString();
    }

    // Update is called once per frame
    void Update() {
        
    }



    public void UpdateUpgradeMenu(Transform newTarget) {
        currentTarget = newTarget;
        //See what you're looking at
        if (currentTarget.tag == "Tower") {
            //If another turret, just change costs
            UpdateCosts();
            StartCoroutine(SwapToTowerMenu());

        }
        else if (currentTarget.tag == "Home") {
            StartCoroutine(SwapToGardenMenu());
        }
    }

    private IEnumerator SwapToGardenMenu() {
        //Make menu go away
        towerUpgradeMenuContainer.DOAnchorPosY(-130f, 1f);
        yield return new WaitForSeconds(1.2f);
        //Make changes
        towerUpgradeMenuContainer.gameObject.SetActive(false);
        //gardenMenuContainer.anchoredPosition.y = -130f;
        gardenMenuContainer.gameObject.SetActive(true);
        //Make new menu appear
        gardenMenuContainer.DOAnchorPosY(0f, 1f);
    }

    private IEnumerator SwapToTowerMenu() {
        //Make menu go away
        gardenMenuContainer.DOAnchorPosY(-230f, 1f);
        yield return new WaitForSeconds(1.1f);
        //Make changes
        towerUpgradeMenuContainer.gameObject.SetActive(true);
        gardenMenuContainer.gameObject.SetActive(false);
        //Make new menu appear
        towerUpgradeMenuContainer.DOAnchorPosY(0f, 1f);
    }

    public void SpawnNewTower() {
        if (currency > newTowerCost) {
            Debug.Log("Attempting to spawn new tower");

            for (int i = 0; i < spawnPoints.Count; i++) {
                Collider towerCollider = null;
                Collider[] colliders = Physics.OverlapSphere(spawnPoints[i].position, 0.4f);

                for (int j = 0; j < colliders.Length; j++) {
                    if (colliders[j].tag == "Tower") {
                        towerCollider = colliders[j];
                        Debug.Log("Tower intercepted: " + towerCollider.name);
                        break;
                    }
                }

                if (towerCollider == null) {
                    Debug.Log("Spawning in new tower");
                    GameObject newTower = Instantiate(towerPrefab, spawnPoints[i].position, spawnPoints[i].rotation);
                    cameraController.turretTransforms.Add(newTower.transform);
                    currency -= newTowerCost;
                    break;
                }
            }
        }
        else {
            Debug.Log("Not enough money");
        }
    }

    public void UpdateCosts() {
        currentTower = currentTarget.GetComponent<Tower>();
        if (currentTower != null) {
            rangeCostTMPro.text = currentTower.upgradeCost.ToString();
            fireRateCostTMPro.text = currentTower.upgradeCost.ToString();
            damageCostTMPro.text = currentTower.upgradeCost.ToString();
        }
    }

    public void UpgradeRange() {
        if (currentTower != null && currency > 50) {
            currentTower.range *= 1.1f;
            currentTower.upgradeCost += 50;
            currency -= 50;
            UpdateCosts();
        }
    }

    public void UpgradeFireRate() {
        if (currentTower != null && currency > 50) {
            currentTower.fireRate *= 1.1f;
            currentTower.upgradeCost += 50;
            currency -= 50;
            UpdateCosts();
        }
    }

    public void UpgradeDamage() {
        if (currentTower != null && currency > 50) {
            currentTower.damage *= 1.1f;
            currentTower.upgradeCost += 50;
            currency -= 50;
            UpdateCosts();
        }
    }
}
