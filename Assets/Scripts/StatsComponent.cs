using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsComponent : MonoBehaviour
{
    public float maxHealth = 1.0f, speed = 10f, damage = .1f;
    private float currHealth;

    public float Health
    {
        get
        {
            return currHealth / maxHealth;
        }

        set
        {
            currHealth = Mathf.Clamp(currHealth += maxHealth, 0.0f, maxHealth);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        currHealth = maxHealth;
    }
}
