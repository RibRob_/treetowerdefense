using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Track : MonoBehaviour
{
    //[SerializeField] private List<List<Transform>> trackPoints = new List<List<Transform>>();

    [SerializeField] private List<Curve> myTrack = new List<Curve>();

    List<Vector3> allPoints = new List<Vector3>();

    public List<Vector3> getAllPoints() { return allPoints; }

    public Transform finaltarget;

    private void Start()
    {
        foreach (Curve curves in myTrack)
        {
            foreach (Transform t in curves.getPoints())
            {
                allPoints.Add(t.position);
            }
        }

        allPoints.Add(finaltarget.position);
    }
}
