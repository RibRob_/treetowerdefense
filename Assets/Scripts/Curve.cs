using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Curve : MonoBehaviour
{
    [SerializeField] private GameObject prefab;

    [SerializeField] private Transform pA;
    [SerializeField] private Transform pB;
    [SerializeField] private Transform pC;
    [SerializeField] private Transform pD;

    private Vector3 pAB;
    private Vector3 pBC;
    private Vector3 pCD;

    private Vector3 pAB_BC;

    private Vector3 pBC_CD;

    //[SerializeField] private Transform pABCD;

    private float interpolateAmount;

    List<Transform> points = new List<Transform>();

    public List<Transform> getPoints() { return points; }


    private void Awake()
    {
        for (int i = 1; i <= 100; i++)
        {
            interpolateAmount = i / 100f;

            pAB = Vector3.Lerp(pA.position, pB.position, interpolateAmount);
            pBC = Vector3.Lerp(pB.position, pC.position, interpolateAmount);
            pCD = Vector3.Lerp(pC.position, pD.position, interpolateAmount);

            pAB_BC = Vector3.Lerp(pAB, pBC, interpolateAmount);
            pBC_CD = Vector3.Lerp(pBC, pCD, interpolateAmount);

            points.Add(Instantiate(prefab, Vector3.Lerp(pAB_BC, pBC_CD, interpolateAmount), Quaternion.identity).transform);
        }
    }

    private void Update()
    {
        for (int i = 0; i < 100; i++)
        {
            interpolateAmount = i / 100f;

            pAB = Vector3.Lerp(pA.position, pB.position, interpolateAmount);
            pBC = Vector3.Lerp(pB.position, pC.position, interpolateAmount);
            pCD = Vector3.Lerp(pC.position, pD.position, interpolateAmount);

            pAB_BC = Vector3.Lerp(pAB, pBC, interpolateAmount);
            pBC_CD = Vector3.Lerp(pBC, pCD, interpolateAmount);

            points[i].position = (Vector3.Lerp(pAB_BC, pBC_CD, interpolateAmount));
        }
    }

    /*
    void Update()
    {
        interpolateAmount = (interpolateAmount + Time.deltaTime/20) % 1;

        pAB = Vector3.Lerp(pA.position, pB.position, interpolateAmount);
        pBC = Vector3.Lerp(pB.position, pC.position, interpolateAmount);
        pCD = Vector3.Lerp(pC.position, pD.position, interpolateAmount);

        pAB_BC = Vector3.Lerp(pAB, pBC, interpolateAmount);
        pBC_CD = Vector3.Lerp(pBC, pCD, interpolateAmount);

        pABCD.position = Vector3.Lerp(pAB_BC, pBC_CD, interpolateAmount);
    }
    */
}
